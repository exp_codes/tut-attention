from collections import Counter

from keras import Model
from keras.layers import Dense, Reshape

import src.utility as utility
import src.models as models

import os
import numpy as np
import matplotlib.pyplot as plt
from os.path import isfile

from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras.optimizers import Adam

from sklearn.metrics import confusion_matrix, classification_report
import sys

sys.path.append('../')
from src.config import artist20_dataset_dir_path, song_data, use_GRU, use_CNN, use_CRNN


def train_model(nb_classes=20,
                slice_length=911,
                artist_folder=artist20_dataset_dir_path,
                song_folder=song_data,
                plots=True,
                train=True,
                load_checkpoint=False,
                save_metrics=True,
                save_metrics_folder='metrics',
                save_weights_folder='weights',
                batch_size=16,
                nb_epochs=200,
                early_stop=2,
                lr=0.0001,
                album_split=True,
                random_states=42):
    """
    Main function for training the model and testing
    """

    weights = os.path.join(save_weights_folder, str(nb_classes) +
                           '_' + str(slice_length) + '_' + str(random_states))
    os.makedirs(save_weights_folder, exist_ok=True)
    os.makedirs(save_metrics_folder, exist_ok=True)

    print("Loading dataset...")

    if not album_split:
        # song split
        Y_train, X_train, S_train, Y_test, X_test, S_test, \
        Y_val, X_val, S_val = \
            utility.load_dataset_song_split(song_folder_name=song_folder,
                                            artist_folder=artist_folder,
                                            nb_classes=nb_classes,
                                            random_state=random_states)
    else:
        Y_train, X_train, S_train, Y_test, X_test, S_test, \
        Y_val, X_val, S_val = \
            utility.load_dataset_album_split(song_folder_name=song_folder,
                                             artist_folder=artist_folder,
                                             nb_classes=nb_classes,
                                             random_state=random_states)

    print("Loaded and split dataset. Slicing songs...")

    # Create slices out of the songs
    X_train, Y_train, S_train = utility.slice_songs(X_train, Y_train, S_train,
                                                    length=slice_length)
    X_val, Y_val, S_val = utility.slice_songs(X_val, Y_val, S_val,
                                              length=slice_length)
    X_test, Y_test, S_test = utility.slice_songs(X_test, Y_test, S_test,
                                                 length=slice_length)

    print("Training set label counts:", np.unique(Y_train, return_counts=True))

    # Encode the target vectors into one-hot encoded vectors
    Y_train, le, enc = utility.encode_labels(Y_train)
    Y_test, le, enc = utility.encode_labels(Y_test, le, enc)
    Y_val, le, enc = utility.encode_labels(Y_val, le, enc)

    # Reshape data as 2d convolutional tensor shape
    X_train = X_train.reshape(X_train.shape + (1,))
    X_val = X_val.reshape(X_val.shape + (1,))
    X_test = X_test.reshape(X_test.shape + (1,))

    # build the model
    if use_CRNN:
        model = models.define_discriminator_model(X_train.shape, n_classes=nb_classes)
    elif use_CNN:
        model = models.cnn_model(X_train.shape, n_classes=nb_classes)
    elif use_GRU:
        model = models.gru_model(X_train.shape, n_classes=nb_classes)
    else:
        model = ''

    model.compile(loss='categorical_crossentropy',
                  optimizer=Adam(lr=lr),
                  metrics=['accuracy'])
    model.summary()

    # Initialize weights using checkpoint if it exists
    if load_checkpoint:
        print("Looking for previous weights...")
        if isfile(weights):
            print('Checkpoint file detected. Loading weights.')
            model.load_weights(weights)
        else:
            print('No checkpoint file detected.  Starting from scratch.')
    else:
        print('Starting from scratch (no checkpoint)')

    checkpointer = ModelCheckpoint(filepath=weights,
                                   verbose=1,
                                   save_best_only=True)
    earlystopper = EarlyStopping(monitor='val_loss', min_delta=0,
                                 patience=early_stop, verbose=0, mode='auto')

    # Train the model
    if train:
        print("Input Data Shape", X_train.shape)
        history = model.fit(X_train, Y_train, batch_size=batch_size,
                            shuffle=True, epochs=nb_epochs,
                            verbose=1, validation_data=(X_val, Y_val),
                            callbacks=[checkpointer, earlystopper])
        if plots:
            utility.plot_history(history)

    # Load weights that gave best performance on validation set
    model.load_weights(weights)
    filename = os.path.join(save_metrics_folder, str(nb_classes) + '_'
                            + str(slice_length)
                            + '_' + str(random_states) + '.txt')

    # Score test model
    score = model.evaluate(X_test, Y_test, verbose=0)
    # y_score = model.predict_proba(X_test)
    y_score = model.predict(X_test)

    # Calculate confusion matrix
    y_predict = np.argmax(y_score, axis=1)
    y_true = np.argmax(Y_test, axis=1)
    cm = confusion_matrix(y_true, y_predict)

    # Plot the confusion matrix
    class_names = np.arange(nb_classes)
    class_names_original = le.inverse_transform(class_names)
    plt.figure(figsize=(14, 14))
    utility.plot_confusion_matrix(cm, classes=class_names_original,
                                  normalize=True,
                                  title='Confusion matrix with normalization')
    if save_metrics:
        plt.savefig(filename + '.pdf', bbox_inches="tight")
    plt.close()
    plt.figure(figsize=(14, 14))

    # Print out metrics
    print('Test score/loss:', score[0])
    print('Test accuracy:', score[1])
    print('\nTest results on each slice:')

    scores = classification_report(y_true, y_predict,
                                   target_names=class_names_original)
    scores_dict = classification_report(y_true, y_predict,
                                        target_names=class_names_original,
                                        output_dict=True)
    print(scores)

    # Predict artist using pooling methodology
    pooling_scores, pooled_scores_dict = \
        utility.predict_artist(model, X_test, Y_test, S_test,
                               le, class_names=class_names_original,
                               slices=None, verbose=False)

    # Save metrics
    if save_metrics:
        plt.savefig(filename + '_pooled.pdf', bbox_inches="tight")
        plt.close()
        with open(filename, 'w') as f:
            f.write("Training data shape:" + str(X_train.shape))
            f.write('\nnb_classes: ' + str(nb_classes) +
                    '\nslice_length: ' + str(slice_length))
            f.write('\nweights: ' + weights)
            f.write('\nlr: ' + str(lr))
            f.write('\nTest score/loss: ' + str(score[0]))
            f.write('\nTest accuracy: ' + str(score[1]))
            f.write('\nTest results on each slice:\n')
            f.write(str(scores))
            f.write('\n\n Scores when pooling song slices:\n')
            f.write(str(pooling_scores))

    return (scores_dict, pooled_scores_dict)


def use_model(nb_classes=20,
              slice_length=911,
              artist_folder=artist20_dataset_dir_path,
              song_folder=song_data,
              save_weights_folder='weights',
              lr=0.0001,
              random_states=42):
    """
    Main function for use the model
    """

    weights = os.path.join(save_weights_folder, str(nb_classes) +
                           '_' + str(slice_length) + '_' + str(random_states))

    print("Loading dataset...")
    # song split
    Y_train, X_train, S_train, Y_test, X_test, S_test, \
    Y_val, X_val, S_val = \
        utility.load_dataset_song_split(song_folder_name=song_folder,
                                        artist_folder=artist_folder,
                                        nb_classes=nb_classes,
                                        random_state=random_states)

    print("Loaded dataset. Slicing songs...")

    # Create slices out of the songs
    X_train, Y_train, S_train = utility.slice_songs(X_train, Y_train, S_train,
                                                    length=slice_length)
    X_test, Y_test, S_test = utility.slice_songs(X_test, Y_test, S_test,
                                                 length=slice_length)
    X_val, Y_val, S_val = utility.slice_songs(X_val, Y_val, S_val,
                                              length=slice_length)

    print("Training set label counts:", np.unique(Y_train, return_counts=True))

    # Reshape data as 2d convolutional tensor shape
    X_train = X_train.reshape(X_train.shape + (1,))
    X_test = X_test.reshape(X_test.shape + (1,))
    X_val = X_val.reshape(X_val.shape + (1,))

    # build the model
    if use_CRNN:
        model = models.define_discriminator_model(X_train.shape, n_classes=nb_classes)
    elif use_CNN:
        model = models.cnn_model(X_train.shape, n_classes=nb_classes)
    elif use_GRU:
        model = models.gru_model(X_train.shape, n_classes=nb_classes)
    else:
        model = ''

    model.compile(loss='categorical_crossentropy',
                  optimizer=Adam(lr=lr),
                  metrics=['accuracy'])
    model.summary()

    # Initialize weights using checkpoint if it exists

    print("Looking for previous weights...")
    if isfile(weights):
        print('Checkpoint file detected. Loading weights.')
        model.load_weights(weights)
    else:
        print('No checkpoint file detected. ')
    new_model = Model(model.inputs, model.layers[-4].output)
    new_model.summary()
    predictY_train = new_model.predict(X_train)
    predictY_test = new_model.predict(X_test)
    predictY_val = new_model.predict(X_val)

    return (predictY_train, Y_train, S_train, predictY_test, Y_test, S_test, predictY_val, Y_val, S_val)


def build_knn(model, output_size):
    '''
    https://github.com/sorenlind/keras-knn/blob/master/keras-knn.ipynb
    '''
    # Flatten feature vector
    flat_dim_size = np.prod(model.output_shape[1:])
    x = Reshape(target_shape=(flat_dim_size,),
                name='features_flat')(model.output)

    # Dot product between feature vector and reference vectors
    x = Dense(units=output_size,
              activation='linear',
              name='dense_knn',
              use_bias=False)(x)

    classifier = Model(inputs=[model.input], outputs=x)
    return classifier


def normalize_ecnodings(encodings):
    ref_norms = np.linalg.norm(encodings, axis=0)
    return encodings / ref_norms


def get_top_k_res(sort_res, Y_lable, top_k):
    Y_res_lst = []
    for item in sort_res[:top_k]:
        Y_res_lst.append(Y_lable[item[0]])
    all_item_count = {}
    for y_value in Y_res_lst:
        key = np.argmax(y_value, axis=0)
        if key not in all_item_count.keys():
            all_item_count[key] = {"counter": 1, "array": y_value}
        else:
            all_item_count[key]["counter"] += 1
    all_array_times = sorted(all_item_count.items(), key=lambda all_item_count: all_item_count[1]['counter'],
                             reverse=True)

    most = all_array_times[0][1]['array']
    return most


def combine_CRNN_attention_KNN(nb_classes=20,
                               slice_length=911,
                               save_weights_folder='weights',
                               random_states=42,
                               artist_folder=artist20_dataset_dir_path,
                               song_folder=song_data,
                               album_split=True,
                               Top_K=11,
                               save_metrics=True,
                               save_metrics_folder='metrics'
                               ):
    '''
    with using relatively few training examples even for problems with many thousand classes.
    '''

    if not album_split:
        # song split
        Y_train, X_train, S_train, Y_test, X_test, S_test, \
        Y_val, X_val, S_val = \
            utility.load_dataset_song_split(song_folder_name=song_folder,
                                            artist_folder=artist_folder,
                                            nb_classes=nb_classes,
                                            random_state=random_states)
    else:
        Y_train, X_train, S_train, Y_test, X_test, S_test, \
        Y_val, X_val, S_val = \
            utility.load_dataset_album_split(song_folder_name=song_folder,
                                             artist_folder=artist_folder,
                                             nb_classes=nb_classes,
                                             random_state=random_states)

    print("Loaded and split dataset. Slicing songs...")

    # Create slices out of the songs
    X_train, Y_train, S_train = utility.slice_songs(X_train, Y_train, S_train,
                                                    length=slice_length)
    X_val, Y_val, S_val = utility.slice_songs(X_val, Y_val, S_val,
                                              length=slice_length)
    X_test, Y_test, S_test = utility.slice_songs(X_test, Y_test, S_test,
                                                 length=slice_length)

    print("Training set label counts:", np.unique(Y_train, return_counts=True))

    # Encode the target vectors into one-hot encoded vectors
    Y_train, le, enc = utility.encode_labels(Y_train)
    Y_test, le, enc = utility.encode_labels(Y_test, le, enc)
    Y_val, le, enc = utility.encode_labels(Y_val, le, enc)

    # Reshape data as 2d convolutional tensor shape
    X_train = X_train.reshape(X_train.shape + (1,))
    X_val = X_val.reshape(X_val.shape + (1,))
    X_test = X_test.reshape(X_test.shape + (1,))

    weights = os.path.join(save_weights_folder, str(nb_classes) +
                           '_' + str(slice_length) + '_' + str(random_states))

    # build the model
    model = models.define_discriminator_model(X_train.shape, n_classes=nb_classes)

    model.summary()

    # Load weights that gave best performance on validation set
    model.load_weights(weights)
    encoded_songs = model.predict(X_train)
    trained_model = Model(model.input, model.layers[-2].output)
    print("trained_model:")
    trained_model.summary()

    joined_model = build_knn(trained_model, encoded_songs.shape[0])
    print("joined_model:")

    temp_weights = joined_model.get_weights()
    encoded_songs_normalized = normalize_ecnodings(encoded_songs)
    temp_weights[-1] = encoded_songs_normalized.T
    joined_model.set_weights(temp_weights)

    joined_model.summary()

    pre_res = joined_model.predict(X_test)
    final_pre = []
    for pre in pre_res:
        res_dict = {}
        for index, item in enumerate(pre):
            res_dict[index] = item
        sort_res = sorted(res_dict.items(), key=lambda res_dict: res_dict[1], reverse=True)
        poll_res = get_top_k_res(sort_res, Y_train, top_k=Top_K)
        final_pre.append(poll_res)
    final_pre = np.array(final_pre)

    # Calculate confusion matrix
    y_predict = np.argmax(final_pre, axis=1)
    y_true = np.argmax(Y_test, axis=1)
    cm = confusion_matrix(y_true, y_predict)

    # Plot the confusion matrix
    class_names = np.arange(nb_classes)
    class_names_original = le.inverse_transform(class_names)
    plt.figure(figsize=(14, 14))
    utility.plot_confusion_matrix(cm, classes=class_names_original,
                                  normalize=True,
                                  title='Confusion matrix with normalization')
    filename = os.path.join(save_metrics_folder, str(nb_classes) + '_'
                            + str(slice_length)
                            + '_' + str(random_states) + '.txt')
    dir_file_path = os.path.dirname(filename)
    if not os.path.isdir(dir_file_path):
        os.makedirs(dir_file_path)
    if save_metrics:
        plt.savefig(filename + '.pdf', bbox_inches="tight")
    plt.close()
    plt.figure(figsize=(14, 14))

    # Print out metrics

    print('\nTest results on each slice:')

    scores = classification_report(y_true, y_predict,
                                   target_names=class_names_original)
    scores_dict = classification_report(y_true, y_predict,
                                        target_names=class_names_original,
                                        output_dict=True)
    print(scores)

    # Predict artist using pooling methodology
    pooling_scores, pooled_scores_dict = \
        utility.predict_artist(model, X_test, Y_test, S_test,
                               le, class_names=class_names_original,
                               slices=None, verbose=False)

    # Save metrics
    if save_metrics:
        plt.savefig(filename + '_pooled.pdf', bbox_inches="tight")
        plt.close()
        with open(filename, 'w') as f:
            f.write("Training data shape:" + str(X_train.shape))
            f.write('\nnb_classes: ' + str(nb_classes) +
                    '\nslice_length: ' + str(slice_length))
            f.write('\nweights: ' + weights)

            f.write('\nTest results on each slice:\n')
            f.write(str(scores))
            f.write('\n\n Scores when pooling song slices:\n')
            f.write(str(pooling_scores))

    return joined_model


if __name__ == '__main__':
    artist20_dataset_dir_path = '/home/zhangxulong/artist20'
    song_data = '/home/zhangxulong/artist20_song_data'
    for dataset in [  '/home/zhangxulong/singer60']:#,'/home/zhangxulong/artist20',]:
        dataset_song = dataset + '_song_data'
        dataset_name = dataset.split('/')[-1]
        NUM=int(dataset_name[-2:])
        if dataset_name == 'artist20':
            album_split = False
        else:
            album_split = False
        combine_CRNN_attention_KNN(album_split=album_split, save_weights_folder='weights_song_split_%s' % dataset_name, random_states=0,
                                   save_metrics_folder='metrics_knn_%s' % dataset_name, slice_length=32,
                                   artist_folder=dataset, song_folder=dataset_song,nb_classes=NUM)
