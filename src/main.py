# -*- coding: utf-8 -*-

import os
import pandas as pd
import gc
import sys

from src.config import  train_or_not, load_or_not, album_split_on, patience_step, plots_or_not, learning_rate

sys.path.append('../')
import src.trainer as trainer

if __name__ == '__main__':

    '''
    1s 32 frames
    3s 94 frames
    5s 157 frames
    6s 188 frames
    10s 313 frames
    20s 628 frames
    29.12s 911 frames
    '''
    for dataset in [  '/home/zhangxulong/singer60',]:
        dataset_song = dataset + '_song_data'
        dataset_name = dataset.split('/')[-1]

        slice_lengths = [32]#,911, 628, 313, 157, 94,]
        random_state_list =[0]#, 21, 42]
        iterations = 1
        nb_class_nums=[int(dataset_name[-2:])]#[32]#20,30,40,50,60
        summary_metrics_output_folder = 'trials_song_split'
        for nb_num in nb_class_nums:
            for slice_len in slice_lengths:

                scores = []
                pooling_scores = []
                for i in range(iterations):
                    score, pooling_score = trainer.train_model(
                        artist_folder=dataset,
                        song_folder=dataset_song,
                        nb_classes=nb_num,#Nb_class,
                        slice_length=slice_len,
                        lr=learning_rate,
                        train=train_or_not,
                        load_checkpoint=load_or_not,
                        plots=plots_or_not,
                        album_split=album_split_on,
                        random_states=random_state_list[i],
                        save_metrics=True,
                        save_metrics_folder='metrics_song_split_%s'%dataset_name,
                        save_weights_folder='weights_song_split_%s'%dataset_name,
                        early_stop=patience_step)

                    scores.append(score['weighted avg'])
                    pooling_scores.append(pooling_score['weighted avg'])
                    gc.collect()

                os.makedirs(summary_metrics_output_folder, exist_ok=True)

                pd.DataFrame(scores).to_csv(
                    '{}/{}_score.csv'.format(summary_metrics_output_folder, slice_len))

                pd.DataFrame(pooling_scores).to_csv(
                    '{}/{}_pooled_score.csv'.format(
                        summary_metrics_output_folder, slice_len))
