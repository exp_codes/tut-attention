all_dataset_dir_path=[
    '/home/zhangxulong/vocal-6',#上音国音干声数据



    '/home/zhangxulong/singer98',#原始singer98每位歌手20首歌
    '/home/zhangxulong/vocal_unet/singer98',#歌声版singer98每位歌手20首歌

    '/home/zhangxulong/singer60',#原始singer60
    '/home/zhangxulong/vocal_unet/singer60',#歌声版singer60

    '/home/zhangxulong/singer32',#原始singer32每位歌手70首歌
    '/home/zhangxulong/vocal_unet/singer32',#歌声版singer32


    '/home/zhangxulong/artist20',#原始artist20
    '/home/zhangxulong/vocal_unet/artist20',#歌声版artist20
]
all_song_data_path=[
    '/home/zhangxulong/vocal-6_song_data',#上音国音干声数据（可用7,1234589）

    '/home/zhangxulong/singer98_song_data',#原始singer98
    '/home/zhangxulong/vocal_unet/singer98_song_data',#歌声版singer98

    '/home/zhangxulong/singer60_song_data',#原始singer60
    '/home/zhangxulong/vocal_unet/singer60_song_data',#歌声版singer60

    '/home/zhangxulong/singer32_song_data',#原始singer32每位歌手70首歌
    '/home/zhangxulong/vocal_unet/singer32_song_data',#歌声版singer32

    '/home/zhangxulong/artist20_song_data',#原始artist20
    '/home/zhangxulong/vocal_unet/artist20_song_data',#歌声版artist20
]



artist20_dataset_dir_path='/home/zhangxulong/artist20'
song_data='/home/zhangxulong/artist20_song_data'
use_attention=False
train_or_not=False
load_or_not=True
album_split_on=False
patience_step=2
plots_or_not=False
learning_rate=0.001


use_CRNN=True
use_CNN=False
use_GRU=False
'''
  1s 32 frames
  3s 94 frames
  5s 157 frames
  6s 188 frames
  10s 313 frames
  20s 628 frames
  29.12s 911 frames
  '''

