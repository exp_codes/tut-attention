# -*- coding: utf-8 -*-
import scipy
import sklearn
from scipy.spatial.distance import cosine
from sklearn.externals import joblib
import os
import pandas as pd
import gc
import sys
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity

from src.config import Nb_class, train_or_not, load_or_not, album_split_on, patience_step, plots_or_not, learning_rate, \
    artist20_dataset_dir_path, song_data

sys.path.append('../')
import src.trainer as trainer

if __name__ == '__main__':

    '''
    1s 32 frames
    3s 94 frames
    5s 157 frames
    6s 188 frames
    10s 313 frames
    20s 628 frames
    29.12s 911 frames
    '''
    temp_file='temp.joblib.pkl'
    if not os.path.isfile(temp_file):
        predictY_train,Y_train,S_train,predictY_test,Y_test,S_test,predictY_val,Y_val,S_val=trainer.use_model(
            nb_classes=20,
            slice_length=32,
            lr=learning_rate,
            random_states=0,
            save_weights_folder='weights_song_split',
            artist_folder=artist20_dataset_dir_path,
            song_folder=song_data,
            )
        joblib.dump([predictY_train,Y_train,S_train,predictY_test,Y_test,S_test,predictY_val,Y_val,S_val],temp_file)
    else:
        predictY_train,Y_train,S_train,predictY_test,Y_test,S_test,predictY_val,Y_val,S_val=joblib.load(temp_file)

    Y_list=set(Y_train)
    enroll_dict={}
    for y in Y_list:
        enroll_dict[y]=np.mean(predictY_train[np.where(Y_train==y)],axis=0)
    val_song_dict={}
    for s in set(S_val):
        singer=Y_val[np.where(S_val==s)][0]
        feat=np.mean(predictY_val[np.where(S_val==s)],axis=0)
        val_song_dict[s]=[singer,feat]

    max_value=min_value=0
    right=total=0
    for song in val_song_dict.keys():
        ground_truth=val_song_dict[song][0]
        song_feat=val_song_dict[song][1]
        cos_score={}
        for singer in enroll_dict.keys():
            singer_feat=enroll_dict[singer]
            cos_score[singer]=cosine(singer_feat,song_feat)
            if cos_score[singer]>max_value:
                max_value=cos_score[singer]
            if cos_score[singer]<min_value:
                min_value=cos_score[singer]
        most_like=min(cos_score, key=cos_score.get)
        most_unlike=max(cos_score, key=cos_score.get)
        OKGREEN = '\033[92m'
        WARNING = '\033[91m'
        if ground_truth==most_like:
            right+=1
            print_plus=OKGREEN
        else:
            print_plus=WARNING
        total+=1
        print(print_plus+"真实歌手：%s\t最像：%s=%.3f\t最不像：%s=%.3f\033[0m"%(ground_truth,most_like,cos_score[most_like],most_unlike,cos_score[most_unlike]))
        print(cos_score)
    print("right:%d total:%d acc:%.3f"%(right,total,right/total))



    print('test')


    gc.collect()


