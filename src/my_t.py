import sys

from src.unet_wuym import dir_all_convert_to_vocal

sys.path.append('../')
from src.utility import move_dataset, convert_wav_2_mp3, move_wav_out, move_count_dataset

if __name__ == '__main__':
    do_str='5'
    if '1' in do_str:
        move_dataset('/home/zhangxulong/artist20')
    if '2' in do_str:
        convert_wav_2_mp3('/home/zhangxulong/artist20')
    if '3' in do_str:
        move_wav_out('/home/zhangxulong/artist20')
    if '4' in do_str:
        singer98='/home/zhangxulong/singer98'
        move_count_dataset(singer98,20)
        move_count_dataset(singer98,40)
        move_count_dataset(singer98,60)
        move_count_dataset(singer98,80)
    if '5' in do_str:
        dir_all_convert_to_vocal('/home/zhangxulong/vocal_unet')
    if '6' in do_str:
        singer32='/home/zhangxulong/singer32'
        move_count_dataset(singer32,20)
    if '7' in do_str:
        singer32='/home/zhangxulong/vocal_unet/singer32'
        move_count_dataset(singer32,20)