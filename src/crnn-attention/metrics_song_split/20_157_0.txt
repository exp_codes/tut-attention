Training data shape:(54491, 128, 157, 1)
nb_classes: 20
slice_length: 157
weights: weights_song_split/20_157_0
lr: 0.001
Test score/loss: 1.0474784900698215
Test accuracy: 0.690191924571991
Test results on each slice:
                              precision    recall  f1-score   support

                   aerosmith       0.84      0.38      0.52       259
                     beatles       0.68      0.80      0.74       282
creedence_clearwater_revival       0.84      0.95      0.89       249
                        cure       0.69      0.61      0.65       461
          dave_matthews_band       0.76      0.75      0.75       409
                depeche_mode       0.66      0.68      0.67       327
               fleetwood_mac       0.61      0.88      0.72       353
                garth_brooks       0.85      0.96      0.90       265
                   green_day       0.97      0.71      0.82       287
                led_zeppelin       0.45      0.78      0.57       274
                     madonna       0.82      0.64      0.72       396
                   metallica       0.85      0.91      0.88       473
                      prince       0.65      0.75      0.70       286
                       queen       0.56      0.58      0.57       345
                   radiohead       0.60      0.51      0.55       319
                     roxette       0.92      0.34      0.50       392
                  steely_dan       0.67      0.84      0.75       257
                suzanne_vega       0.49      0.62      0.55       287
                   tori_amos       0.71      0.83      0.76       387
                          u2       0.56      0.35      0.43       309

                    accuracy                           0.69      6617
                   macro avg       0.71      0.69      0.68      6617
                weighted avg       0.71      0.69      0.68      6617


 Scores when pooling song slices:
                              precision    recall  f1-score   support

                   aerosmith       1.00      0.60      0.75         5
                     beatles       1.00      1.00      1.00         9
creedence_clearwater_revival       0.86      1.00      0.92         6
                        cure       0.80      1.00      0.89         8
          dave_matthews_band       0.67      0.86      0.75         7
                depeche_mode       1.00      1.00      1.00         7
               fleetwood_mac       0.78      1.00      0.88         7
                garth_brooks       1.00      1.00      1.00         7
                   green_day       1.00      0.89      0.94         9
                led_zeppelin       0.62      1.00      0.77         5
                     madonna       1.00      0.86      0.92         7
                   metallica       0.88      1.00      0.93         7
                      prince       0.86      0.86      0.86         7
                       queen       0.78      0.88      0.82         8
                   radiohead       0.75      0.86      0.80         7
                     roxette       1.00      0.44      0.62         9
                  steely_dan       1.00      1.00      1.00         5
                suzanne_vega       0.80      0.57      0.67         7
                   tori_amos       0.80      1.00      0.89         8
                          u2       1.00      0.33      0.50         6

                    accuracy                           0.86       141
                   macro avg       0.88      0.86      0.85       141
                weighted avg       0.88      0.86      0.85       141
