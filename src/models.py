from keras.layers import Conv2D, MaxPooling2D,Dense, Lambda, dot, Activation, concatenate,Dropout, Reshape, Permute,Input
from keras.layers.normalization import BatchNormalization
from keras.layers.recurrent import GRU
from keras.models import Model
from keras.optimizers import Adam
from keras.utils import plot_model
import sys
sys.path.append('../')
from src.config import use_attention


def attention_3d_block(hidden_states):
    """
    Many-to-one attention mechanism for Keras.
    @param hidden_states: 3D tensor with shape (batch_size, time_steps, input_dim).
    @return: 2D tensor with shape (batch_size, 128)
    @author: felixhao28.
    """
    hidden_size = int(hidden_states.shape[2])
    # Inside dense layer
    #              hidden_states            dot               W            =>           score_first_part
    # (batch_size, time_steps, hidden_size) dot (hidden_size, hidden_size) => (batch_size, time_steps, hidden_size)
    # W is the trainable weight matrix of attention Luong's multiplicative style score
    score_first_part = Dense(hidden_size, use_bias=False, name='attention_score_vec')(hidden_states)
    #            score_first_part           dot        last_hidden_state     => attention_weights
    # (batch_size, time_steps, hidden_size) dot   (batch_size, hidden_size)  => (batch_size, time_steps)
    h_t = Lambda(lambda x: x[:, -1, :], output_shape=(hidden_size,), name='last_hidden_state')(hidden_states)
    score = dot([score_first_part, h_t], [2, 1], name='attention_score')
    attention_weights = Activation('softmax', name='attention_weight')(score)
    # (batch_size, time_steps, hidden_size) dot (batch_size, time_steps) => (batch_size, hidden_size)
    context_vector = dot([hidden_states, attention_weights], [1, 1], name='context_vector')
    pre_activation = concatenate([context_vector, h_t], name='attention_output')
    attention_vector = Dense(128, use_bias=False, activation='tanh', name='attention_vector')(pre_activation)
    return attention_vector

# define the standalone supervised discriminator models
def define_discriminator_model(X_shape, n_classes=20):
    # image input
    #model_name='crnn'

    in_shape = (X_shape[1], X_shape[2], X_shape[3])
    in_image = Input(shape=in_shape)
    nb_layers = 4  # number of convolutional layers
    nb_filters = [64, 128, 128, 128]  # filter sizes
    kernel_size = (3, 3)  # convolution kernel size
    activation = 'elu'  # activation function to use after each layer
    pool_size = [(2, 2), (4, 2), (4, 2), (4, 2),
                 (4, 2)]  # size of pooling area

    # shape of input data (frequency, time, channels)
    frequency_axis = 1
    time_axis = 2
    channel_axis = 3

    # Create sequential model and normalize along frequency axis
    fe = BatchNormalization(axis=frequency_axis, input_shape=in_shape)(in_image)

    # First convolution layer specifies shape
    fe = Conv2D(nb_filters[0], kernel_size=kernel_size, padding='same',
                data_format="channels_last",
                input_shape=in_shape)(fe)
    fe = Activation(activation)(fe)
    fe = BatchNormalization(axis=channel_axis)(fe)
    fe = MaxPooling2D(pool_size=pool_size[0], strides=pool_size[0])(fe)
    fe = Dropout(0.1)(fe)

    # Add more convolutional layers
    for layer in range(nb_layers - 1):
        # Convolutional layer
        fe = Conv2D(nb_filters[layer + 1], kernel_size=kernel_size,
                    padding='same')(fe)
        fe = Activation(activation)(fe)
        fe = BatchNormalization(
            axis=channel_axis)(fe)  # Improves overfitting/underfitting
        fe = MaxPooling2D(pool_size=pool_size[layer + 1],
                          strides=pool_size[layer + 1])(fe)  # Max pooling
        fe = Dropout(0.1)(fe)

        # Reshaping input for recurrent layer


# (frequency, time, channels) --> (time, frequency, channel)
    fe = Permute((time_axis, frequency_axis, channel_axis))(fe)
    resize_shape = fe.shape[2] * fe.shape[3]
    fe = Reshape((fe.shape[1].value, resize_shape.value))(fe)

    # recurrent layer
    fe = GRU(32, return_sequences=True)(fe)
    if use_attention:

        fe = GRU(32, return_sequences=True)(fe)
        fe = attention_3d_block(fe)  # add attention
    else:
        fe = GRU(32, return_sequences=False)(fe)
    fe = Dropout(0.3)(fe)

    d_model=Model(in_image,fe)

    # Output layer
    c_out_layer = Dense(n_classes)(fe)
    # supervised output
    c_out_layer = Activation("softmax")(c_out_layer)

    # define and compile supervised discriminator model
    c_model = Model(in_image, c_out_layer)
    c_model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.0002, beta_1=0.5), metrics=['accuracy'])

    plot_model(c_model, to_file='../figs/crnn-with-attention-%s.pdf'%use_attention,show_shapes=True, show_layer_names=True)

    c_model.summary()
    return c_model

def cnn_model(X_shape, n_classes=20):
    #model_name='cnn'
    # image input

    in_shape = (X_shape[1], X_shape[2], X_shape[3])
    in_image = Input(shape=in_shape)
    nb_layers = 4  # number of convolutional layers
    nb_filters = [64, 128, 128, 128]  # filter sizes
    kernel_size = (3, 3)  # convolution kernel size
    activation = 'elu'  # activation function to use after each layer
    pool_size = [(2, 2), (4, 2), (4, 2), (4, 2),
                 (4, 2)]  # size of pooling area

    # shape of input data (frequency, time, channels)
    frequency_axis = 1
    time_axis = 2
    channel_axis = 3

    # Create sequential model and normalize along frequency axis
    fe = BatchNormalization(axis=frequency_axis, input_shape=in_shape)(in_image)

    # First convolution layer specifies shape
    fe = Conv2D(nb_filters[0], kernel_size=kernel_size, padding='same',
                data_format="channels_last",
                input_shape=in_shape)(fe)
    fe = Activation(activation)(fe)
    fe = BatchNormalization(axis=channel_axis)(fe)
    fe = MaxPooling2D(pool_size=pool_size[0], strides=pool_size[0])(fe)
    fe = Dropout(0.1)(fe)

    # Add more convolutional layers
    for layer in range(nb_layers - 1):
        # Convolutional layer
        fe = Conv2D(nb_filters[layer + 1], kernel_size=kernel_size,
                    padding='same')(fe)
        fe = Activation(activation)(fe)
        fe = BatchNormalization(
            axis=channel_axis)(fe)  # Improves overfitting/underfitting
        fe = MaxPooling2D(pool_size=pool_size[layer + 1],
                          strides=pool_size[layer + 1])(fe)  # Max pooling
        fe = Dropout(0.1)(fe)



    # (frequency, time, channels) --> (time, frequency, channel)
    fe = Permute((time_axis, frequency_axis, channel_axis))(fe)

    if use_attention:
        resize_shape = fe.shape[2] * fe.shape[3]
        fe = Reshape(( fe.shape[1].value,resize_shape.value))(fe)

        fe = attention_3d_block(fe)  # add attention
    else:
        resize_shape = fe.shape[1]*fe.shape[2] * fe.shape[3]
        fe = Reshape(( resize_shape.value,))(fe)

    fe = Dropout(0.3)(fe)

    # Output layer
    fe = Dense(n_classes)(fe)
    # supervised output
    c_out_layer = Activation("softmax")(fe)

    # define and compile supervised discriminator model
    c_model = Model(in_image, c_out_layer)
    c_model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.0002, beta_1=0.5), metrics=['accuracy'])

    plot_model(c_model, to_file='../figs/cnn-with-attention-%s.pdf'%use_attention,show_shapes=True, show_layer_names=True)

    c_model.summary()
    return c_model

def gru_model(X_shape, n_classes=20):
    # image input
    #model_name='gru'

    in_shape = (X_shape[1], X_shape[2], X_shape[3])
    in_image = Input(shape=in_shape)
    activation = 'elu'  # activation function to use after each layer

    # shape of input data (frequency, time, channels)
    frequency_axis = 1
    time_axis = 2
    channel_axis = 3

    # Create sequential model and normalize along frequency axis
    fe = BatchNormalization(axis=frequency_axis, input_shape=in_shape)(in_image)
    # (frequency, time, channels) --> (time, frequency, channel)
    fe = Permute((time_axis, frequency_axis, channel_axis))(fe)
    resize_shape = fe.shape[2] * fe.shape[3]
    fe = Reshape((fe.shape[1].value, resize_shape.value))(fe)

    # recurrent layer
    fe = GRU(32, return_sequences=True)(fe)
    if use_attention:

        fe = GRU(32, return_sequences=True)(fe)
        fe = attention_3d_block(fe)  # add attention
    else:
        fe = GRU(32, return_sequences=False)(fe)
    fe = Dropout(0.3)(fe)

    # Output layer
    fe = Dense(n_classes)(fe)
    # supervised output
    c_out_layer = Activation("softmax")(fe)

    # define and compile supervised discriminator model
    c_model = Model(in_image, c_out_layer)
    c_model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.0002, beta_1=0.5), metrics=['accuracy'])

    plot_model(c_model, to_file='../figs/gru-with-attention-%s.pdf'%use_attention,show_shapes=True, show_layer_names=True)

    c_model.summary()
    return c_model
