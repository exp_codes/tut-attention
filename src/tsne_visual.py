import os

import joblib
import matplotlib.cm as cm
import numpy as np
import tensorflow as tf
from matplotlib import pyplot as plt
from sklearn import (manifold)


def get_data_visualize_tsne_bottle_neck_layer():
    label_encoder = joblib.load(label_encoder_path)

    visual_dir = os.path.join(dataset_dir, 'train')
    all_train_data = []
    all_train_label = []
    for root, dirs, names in os.walk(visual_dir):
        for name in names:
            if '.wav' in name:
                wav_path = os.path.join(root, name)
                wav_data, sample_rate = process_wav_wavdata(wav_path)
                label = wav_path.split(os.sep)[3]
                data_x = []
                for item_data in range(0, len(wav_data), int(wav_dur * sample_rate)):
                    seg_wav_data = wav_data[item_data:int(item_data + wav_dur * sample_rate)]
                    if len(seg_wav_data) < wav_dur * sample_rate:
                        continue
                    data_x.append(seg_wav_data)

                res_bottle_neck_layer = wnc.bottle_neck_layer.predict(np.array(data_x))
                all_train_data.append(np.median(res_bottle_neck_layer, axis=0))

                all_train_label.append(label_encoder.transform([label])[0])

    return np.array(all_train_data), all_train_label


def visualization(X, y, figure_name="tsne"):
    label_encoder = joblib.load(label_encoder_path)
    # t-SNE embedding of the digits dataset
    print("Computing t-SNE embedding")
    tsne = manifold.TSNE(n_components=2, init='pca', random_state=0)
    X_tsne = tsne.fit_transform(X)

    # Scale and visualize the embedding vectors
    x_min, x_max = np.min(X_tsne, 0), np.max(X_tsne, 0)
    X_tsne = (X_tsne - x_min) / (x_max - x_min)
    colors = cm.rainbow(np.linspace(0, 1, len(set(y))))
    plt.figure()
    for i in range(X_tsne.shape[0]):
        plt.plot(X_tsne[i, 0], X_tsne[i, 1], 'o', color=colors[y[i]])
    plt.axis('off')

    for y_item in set(y):
        all_x_loc = []
        for x_loc, y_label in zip(X_tsne, y):
            if y_label == y_item:
                all_x_loc.append(x_loc)
        mean_x_loc = np.median(all_x_loc, 0)
        y_item_label = label_encoder.inverse_transform([y_item])[0]
        plt.annotate(y_item_label, mean_x_loc,
                     color='black')  # ,backgroundcolor=colors[y_item])

    plt.savefig(figure_name + '.svg', format="svg", bbox_inches='tight')
    # cmd_str = '"C:\Program Files\Inkscape\inkscape.exe" -D -z --file={0}.svg --export-pdf={0}.pdf'.format(figure_name)
    # print('inkscape svg to pdf...')
    # os.system(cmd_str)

    return 0


if __name__ == '__main__':
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    print('view...')
    start_point = 22
    end_point = 27
    x, y = get_data_visualize_tsne_bottle_neck_layer()
    visualization(x, y, 'img/tsne-new')
